# Dancing Laminette

Dancing Laminette is an interactive installation I did as a maker in residence in [Nordenfjeldske Kunstindustrimuseum](http://nkim.no/en) in Trondheim, Norway, from 18th till 30th August 2016. Laminette is a famous Norwegian chair designed Sven Ivar Dysthe in 1968.

Installation consists of a (modified) chair with motors integrated into its legs and a Raspberry Pi 3 computer with camera running the face detection algorithm. When there are no detectable faces in the field of view chair starts to whistle/sing and "dance" (move its legs). As soon as a face is detected chair stops in the normal position.

This repository contains all code and 3D files.


### Details

System is written in Python and uses OpenCV for face detection. Pigpio is used for servo motor control. Main application runs three processes: one for fetching the video stream from the camera, one for image processing and one for servo control. This way each proces runs on separate CPU core (this isn't possible, at least not easily, using threads in Python due to the GIL).

Besides the main application there is also web interface for configuration/debugging which shows positions of detected faces, stream from the camera  and can set some options. Raspberry is configured to act as a Wi-Fi access point so configuration is done by connecting to it and opening the web interface. Backed is written using python-bottle and frontend is a simple HTML with some jQuery.

System is also modified to boot from the read-only file system so power off is possible without proper shutdown.

### License

Code and 3D files are released under the MIT license.

Copyright (c) 2016 by Igor Brkić

