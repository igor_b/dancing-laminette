<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>chair config</title>
    <link rel="stylesheet" href="/static/bootstrap.min.css">
    <script type="text/javascript" src="/static/jquery-1.11.3.min.js"></script>
    <style type="text/css">
      #faces-live{
        width:100%;
        max-width:800px;
        margin:0 auto;
        border:1px solid black;
        display:block;
      }
      #still{
        width:auto;
        max-width: 80%;
        height:auto;
        display:block;
        margin:0 auto;
        border:1px solid black;
        box-shadow: 0 0 3px #aaa;
      }
      #loading{
        position:absolute;
        right:0;
        top:0;
      }
      .container{ position:relative; }
    </style>

    <script type="text/javascript">
    $(function(){
      var data = {};
      $(window).on('resize', function(){
        var w = $('#faces-live').width();
        var h = w/1.33;
        $('#faces-live').height(parseInt(h)+'px');
      });
      $(window).resize();

      var live = $('#live').is(':checked');
      var mode = '';

      $('#live').on('change', function(){
        live = $(this).is(':checked');
        if(live){
          if(mode=='running') refresh_live();
          else if(mode=='stopped') refresh_still();
        }
      });

      var refresh_still = function(){
        $.get('/still', function(d){
          $('#still').attr('src', d)
          if(live && mode=='stopped'){
            setTimeout(refresh_still, 100);
          }
        });
      };

      var refresh_live = function(){
        $.get('/faces', function(d){
          var cnv = document.getElementById('faces-live');
          if(cnv===null) return;
          var ctx = cnv.getContext("2d");
          var w = cnv.width;
          var h = cnv.height;
          ctx.clearRect(0, 0, w, h);
          ctx.stokeStyle="#00AA00";
          for(var i in d.d){
            var r = d.d[i]
            var px = r.x*w;
            var py = r.y*h;
            ctx.moveTo(px,py-20);
            ctx.lineTo(px,py+20);
            ctx.moveTo(px-20,py);
            ctx.lineTo(px+20,py);
            ctx.stroke();
            ctx.beginPath()
            ctx.arc(px,py,(r.w*w+r.h*h)/2,0,2*Math.PI);
            ctx.stroke();
          }

          if(live && mode=='running'){
            setTimeout(refresh_live, 50);
          }
        });
      };


      var refresh = function(){
        $.get('/status', function(d){
          if(d!=mode){
            if(d=='running'){
              $('#status').html('running');
              $('#btn_start').prop('disabled', true);
              $('#btn_stop').prop('disabled', false);
              $('#box-faces').show();
              $('#box-still').hide();
              refresh_live();
            }
            else{
              $('#status').html('stopped');
              $('#btn_start').prop('disabled', false);
              $('#btn_stop').prop('disabled', true);
              $('#box-faces').hide();
              $('#box-still').show();
              refresh_still();
            }
            mode=d;
            $(window).resize();
          }
        });
      };

      refresh();

      $('.ctl').on('click', function(){
        $('#status').html('...');
        $('#btn_start').prop('disabled', true);
        $('#btn_stop').prop('disabled', true);
        $('#box-faces').hide();
        $('#box-still').hide();
        mode='';
        var lv = live;
        live = false;

        switch($(this).attr('id')){
        case 'btn_start':
          $('#loading').show();
          $.get('/start', function(d){
            $('#loading').hide();
            live = lv;
            refresh();
          });
          console.log('start');
          break;
        case 'btn_stop':
          $('#loading').show();
          $.get('/stop', function(d){
            $('#loading').hide();
            live = lv;
            refresh();
          });
          console.log('stop');
          break;
        }
      });
        
    });
    </script>

  </head>
  <body>
    <div class="container">
      <h1>chair config</h1>
      <hr />
      <div style="position:relative">
        <div style="display:none;" id="loading">loading...</div>
        <h3>Status</h3>
        <p class="text-muted">
          Current status: <strong id="status" class="text-primary">...</strong>.
        </p>
        <div class="app_ctl">
          <button class="ctl btn btn-success btn-lg btn-block" disabled id="btn_start">Start</button>
          <button class="ctl btn btn-danger btn-lg btn-block"  disabled id="btn_stop">Stop</button>
          <a href="/advanced" class="btn btn-link btn-block" style="text-align:left;">Advanced settings</a>
        </div>
      </div>
      <hr />

      <div class="checkbox">
        <label>
          <input id="live" type="checkbox" checked> Live update
        </label>
      </div>

      <div style="display:none;" id="box-faces" class="box">
        <canvas width="640" height="480" id="faces-live"></canvas>
      </div>

      <div style="display:none;" id="box-still" class="box">
        <img src="/still/still.jpg" id="still">
      </div>

    </div>
    <footer class="text-muted text-small">
      <hr>
      <p>Nordenfjeldske Kunstindustrimuseum Trondheim</p>
      <p>Chair installation by Igor Brkić &lt;<a href="mailto:igor@hyperglitch.com">igor@hyperglitch.com</a>&gt;</p>

    </footer>
  </body>
</html>
