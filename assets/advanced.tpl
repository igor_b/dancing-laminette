<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>chair advanced config</title>
    <link rel="stylesheet" href="/static/bootstrap.min.css">
    <script type="text/javascript" src="/static/jquery-1.11.3.min.js"></script>
    <style type="text/css">
    </style>

    <script type="text/javascript">
      $(function(){
        var curr='';
        var mode = function(){
          $.get('/mode', function(d){
            curr=d;
            $('#mode').html(d);
            $('#rw').html(d=='read-only'?'Switch to read-write mode':'Switch to read-only mode');
            $('#submit').prop('disabled', curr=='read-only');
            if(curr=='read-only') $('#note').hide();
            else $('#note').show();
          });
        };  
        mode();

        $('#rw').on('click', function(){
          var url = curr=='read-only'?'rw':'ro';
          $.get('/mode/'+url, function(){
            mode();
          });
          return false;
        });
      });
      
    </script>

  </head>
  <body>
    <div class="container">
      <h1>chair advanced settings</h1>
      <hr />
      % if get('msg', False):
        <div class="bg-success"><p>{{msg}}</p></div>
      % end
      <form action="/advanced" method="POST">
        <h3>Image</h3>
        <div class="form-group">
          <label>Size</label>
          <div class="row">
            <div class="col-xs-6"><input type="number" class="form-control" name="width" placeholder="640" value="{{width}}"></div> 
            <div class="col-xs-6"><input type="number" class="form-control" name="height" placeholder="480"value="{{height}}"></div>
          </div>
        </div>

        <h3>Haar detector</h3>
        <div class="form-group">
          <label>Cascade</label>
          <input type="text" class="form-control" name="cascade" value="{{cascade}}">
        </div>
        <div class="form-group">
          <label>Scale factor</label>
          <input type="text" class="form-control" name="scale_factor" value="{{scale_factor}}">
        </div>
        <div class="form-group">
          <label>Minimum number of neighbors</label>
          <input type="text" class="form-control" name="min_neighbors" value="{{min_neighbors}}">
        </div>
        <div class="form-group">
          <label>Minimum block size</label>
          <input type="text" class="form-control" name="min_size" value="{{min_size}}">
        </div>

        <hr>

        <div>
          <p class="text-primary">Notice: settings will be applied when the app is restarted. To save the settings system must be in read-write mode.</p>
          <p class="text-primary">Current mode: <span id="mode" class="text-danger">...</span> mode</p>
          <p id="note" style="display:none" class="text-danger">Note: system should be mounted read-only before power off!!!</p>
        </div>

        <button id="rw" class="btn btn-sm btn-danger">Mount system read-write</button>
        <input id="submit" type="submit" value="Save options" disabled class="btn btn-sm btn-primary">
        <a href="/" class="btn btn-lg btn-link">Back</a>
      </form>

    </div>
    <footer class="text-muted text-small">
      <hr>
      Chair installation by Igor Brkić &lt;<a href="mailto:igor@hyperglitch.com">igor@hyperglitch.com</a>&gt;

    </footer>
  </body>
</html>
