#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pigpio
import time
import re
import subprocess
import settings

pi = pigpio.pi()
sn = ('fr', 'fl', 'br', 'bl')
servos = {
    'fr': {
        'pin': 17, 
        'min': 1150,
        'max': 1950,
        'invert': False
      },
    'fl': {
        'pin': 27, 
        'min': 1100,
        'max': 1950,
        'invert': False
      },
    'br': {
        'pin': 24, 
        'min': 1150,
        'max': 1950,
        'invert': False
      },
    'bl': {
        'pin': 23,
        'min': 1050,
        'max': 1850,
        'invert': False
      }
  }

dances = {
    'whistle1': {
        'sound': 'whistle1.wav',
        'repeat': 3,
        'dance': [
            #                                           |                                           |
            '-0.8 p2 -0.5 p2  -1 p3 0.8 p2 0.5 p2  1  p3 -0.8 p2 -0.5 p2  -1 p3 0.8 p2 0.5 p2  1 p3  0.0 p2  111 p2 111 p3 111 p2 111 p2 111 p3  111 p2  111 p2 111 p3 111 p2 111 p2 111 p3  ',
            '-0.8 p2 -0.5 p2  -1 p3 0.8 p2 0.5 p2  1  p3 -0.8 p2 -0.5 p2  -1 p3 0.8 p2 0.5 p2  1 p3  0.0 p2  111 p2 111 p3 111 p2 111 p2 111 p3  111 p2  111 p2 111 p3 111 p2 111 p2 111 p3  ',
            ' 0.0 p2  111 p2 111 p3 111 p2 111 p2 111 p3 111 p2  111 p2 111 p3 111 p2 111 p2 111 p3 -0.8 p2 -0.5 p2  -1 p3 0.8 p2 0.5 p2  1 p3  -0.8 p2 -0.5 p2  -1 p3 0.8 p2 0.5 p2  1 p3 ',
            ' 0.0 p2  111 p2 111 p3 111 p2 111 p2 111 p3 111 p2  111 p2 111 p3 111 p2 111 p2 111 p3 -0.8 p2 -0.5 p2  -1 p3 0.8 p2 0.5 p2  1 p3  -0.8 p2 -0.5 p2  -1 p3 0.8 p2 0.5 p2  1 p3 ',
          ]
      },
    'whistle2': {
        'sound': 'whistle2.wav',
        'repeat': 3,
        'dance': [
            #                                                        |                                                       |                                                        |
            '0.8 p3 111 p3 111 p3 111 p3 111 p3 111 p3 111 p3 111 p3  0.0 p3 111 p3 111 p3 111 p3 111 p3 111 p3 111 p3 111 p3 -0.8 p3 111 p3 111 p3 111 p3 111 p3 111 p3 111 p3 111 p3 0.0 p3 111 p3 111 p3 111 p3 111 p3 111 p3 111 p3 111 p3',
            '0.0 p3 111 p3 0.8 p3 111 p3 111 p3 111 p3 111 p3 111 p3  111 p3 111 p3 0.0 p3 111 p3 111 p3 111 p3 111 p3 111 p3 111 p3 111 p3 -0.8 p3 111 p3 111 p3 111 p3 111 p3 111 p3 111 p3 111 p3 0.0 p3 111 p3 111 p3 111 p3 111 p3 111 p3',
            '0.0 p3 111 p3 111 p3 111 p3 0.8 p3 111 p3 111 p3 111 p3  111 p3 111 p3 111 p3 111 p3 0.0 p3 111 p3 111 p3 111 p3 111 p3 111 p3 111 p3 111 p3 -0.8 p3 111 p3 111 p3 111 p3 111 p3 111 p3 111 p3 111 p3 0.0 p3 111 p3 111 p3 111 p3',
            '0.0 p3 111 p3 111 p3 111 p3 111 p3 111 p3 0.8 p3 111 p3  111 p3 111 p3 111 p3 111 p3 111 p3 111 p3 0.0 p3 111 p3 111 p3 111 p3 111 p3 111 p3 111 p3 111 p3 -0.8 p3 111 p3 111 p3 111 p3 111 p3 111 p3 111 p3 111 p3 0.0 p3 111 p3'
          ]
      },
    'pam': {
        'sound': 'pam.wav',
        'repeat': 3,
        'dance': [
            '0.9 p3 111 p3 0.0 p3 111 p3 111 p3 111 p3 111 p3 111 p3  -0.9 p3 111 p3 0.0 p3 111 p3 111 p3 111 p3 111 p3 111 p3',
            '0.0 p3 111 p3 111 p3 111 p3 0.9 p3 111 p3 0.0 p3 111 p3  0.0 p3 111 p3 111 p3 111 p3 -0.9 p3 111 p3 0.0 p3 111 p3',
            '0.0 p3 111 p3 111 p3 111 p3 0.9 p3 111 p3 0.0 p3 111 p3  0.0 p3 111 p3 111 p3 111 p3 -0.9 p3 111 p3 0.0 p3 111 p3',
            '0.9 p3 111 p3 0.0 p3 111 p3 111 p3 111 p3 111 p3 111 p3  -0.9 p3 111 p3 0.0 p3 111 p3 111 p3 111 p3 111 p3 111 p3',
          ]
      },
    'kazzoo1': {
        'sound': 'kazzoo1.wav',
        'repeat': 2,
        'dance': [
            #                           |                           |                           |                           |                            #                           |                           |                           |                           |
            '0.3 p1 111 p1 111 p1 111 p1 0.6 p1 111 p1 111 p1 111 p1 0.9 p1 111 p1 111 p1 111 p1 0.6 p1 111 p1 111 p1 111 p1 0.3 p1 111 p1 111 p1 111 p1   -0.3 p1 111 p1 111 p1 111 p1 -0.6 p1 111 p1 111 p1 111 p1 -0.9 p1 111 p1 111 p1 111 p1 -0.6 p1 111 p1 111 p1 111 p1 -0.3 p1 111 p1 111 p1 111 p1    ',
            '111 p1 0.3 p1 111 p1 111 p1 111 p1 0.6 p1 111 p1 111 p1 111 p1 0.9 p1 111 p1 111 p1 111 p1 0.6 p1 111 p1 111 p1 111 p1 0.3 p1 111 p1 111 p1   111 p1 -0.3 p1 111 p1 111 p1 111 p1 -0.6 p1 111 p1 111 p1 111 p1 -0.9 p1 111 p1 111 p1 111 p1 -0.6 p1 111 p1 111 p1 111 p1 -0.3 p1 111 p1 111 p1    ',
            '111 p1 111 p1 0.3 p1 111 p1 111 p1 111 p1 0.6 p1 111 p1 111 p1 111 p1 0.9 p1 111 p1 111 p1 111 p1 0.6 p1 111 p1 111 p1 111 p1 0.3 p1 111 p1   111 p1 111 p1 -0.3 p1 111 p1 111 p1 111 p1 -0.6 p1 111 p1 111 p1 111 p1 -0.9 p1 111 p1 111 p1 111 p1 -0.6 p1 111 p1 111 p1 111 p1 -0.3 p1 111 p1    ',
            '111 p1 111 p1 111 p1 0.3 p1 111 p1 111 p1 111 p1 0.6 p1 111 p1 111 p1 111 p1 0.9 p1 111 p1 111 p1 111 p1 0.6 p1 111 p1 111 p1 111 p1 0.3 p1   111 p1 111 p1 111 p1 -0.3 p1 111 p1 111 p1 111 p1 -0.6 p1 111 p1 111 p1 111 p1 -0.9 p1 111 p1 111 p1 111 p1 -0.6 p1 111 p1 111 p1 111 p1 -0.3 p1    ',
          ]
      }

  }

def dance(d):

    pause_quant = 0.2
    sng = dances[d]
    cor = [
        re.split(' +', sng['dance'][0]),
        re.split(' +', sng['dance'][1]),
        re.split(' +', sng['dance'][2]),
        re.split(' +', sng['dance'][3])
      ]

    # play sound
    # ...
    snd = subprocess.Popen(('aplay', settings.BASEPATH+'sounds/'+sng['sound']))

    for r in range(int(sng['repeat'])):
        stop = 0
        idxs = [-1, -1, -1, -1]
        cont = [time.time(), time.time(), time.time(), time.time()]
        while True:
            if stop==4:
                break

            tm = time.time()
            for si in range(4):
                if tm>cont[si]:
                    idxs[si] += 1
                    s = servos[sn[si]]
                    if idxs[si]>=len(cor[si]):
                        stop += 1
                    else:
                        el = cor[si][idxs[si]].strip()
                        if len(el)>0:
                            if el[0]=='p':
                                # create a pause
                                cont[si] = tm+float(el[1:])*pause_quant
                            else:
                                # add cmd
                                pos = float(el)
                                if s['invert']:
                                    pos *= -1
                                if pos<-1 or pos>1:
                                    final = 0
                                else:
                                    final = (s['max']-s['min'])*(pos/2.0+0.5)+s['min']
                                pi.set_servo_pulsewidth(s['pin'], final)
                                #print("pin: %d, pos: %.2f, final: %d"%(s['pin'], pos, final))
                else:
                    time.sleep(0.01)

    snd.kill()

dance('kazzoo1')
#dance('pam')


# detach
for ss in sn:
    s = servos[ss]
    pi.set_servo_pulsewidth(s['pin'], 1500)
time.sleep(0.5)
for ss in sn:
    s = servos[ss]
    pi.set_servo_pulsewidth(s['pin'], 0)

