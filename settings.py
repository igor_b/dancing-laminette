#!/usr/bin/env python
# -*- coding: utf-8 -*-	

import json
import logging
import sys

BASEPATH = '/home/pi/chair/'
SETTINGS = BASEPATH+'settings.json'
FACES_JSON = '/tmp/faces.json'

LOGLEVEL = 'DEBUG'
LOGFILE = None

FORMAT = '%(asctime)-15s %(message)s'
if LOGFILE is not None:
    logging.basicConfig(filename=LOGFILE, level=LOGLEVEL, format=FORMAT)
else:
    logging.basicConfig(stream=sys.stdout, level=LOGLEVEL, format=FORMAT)


def load_json(fname, array=False):
    try:
        with open(fname, "r") as f:
            return json.load(f)
    except (IOError, ValueError):
        raise ValueError

def save_json(fname, data):
    with open(fname, "w") as f:
        json.dump(data, f, indent=2, separators=(',', ': '))

