#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This file is part of the "Dancing Laminette" interactive installation.
# It is released under MIT license
# Copyright (c) 2016 by Igor Brkić <igor@hyperglitch.com>

from picamera.array import PiRGBArray
from picamera import PiCamera
import time
import re
import os
import sys
import cv2
import multiprocessing
import subprocess
from Queue import Full, Empty
import signal
import pigpio
import random

import settings

global stop_all
stop_all = False

global config
config = {}

sn = ('fr', 'fl', 'br', 'bl')
servos = {
    'fr': {
        'pin': 17, 
        'min': 1150,
        'max': 1950,
        'invert': False
      },
    'fl': {
        'pin': 27, 
        'min': 1100,
        'max': 1950,
        'invert': False
      },
    'br': {
        'pin': 24, 
        'min': 1150,
        'max': 1950,
        'invert': False
      },
    'bl': {
        'pin': 23,
        'min': 1050,
        'max': 1850,
        'invert': False
      }
  }

dances = {
    'whistle1': {
        'sound': 'whistle1.wav',
        'repeat': 3,
        'dance': [
            #                                           |                                           |
            '-0.8 p2 -0.5 p2  -1 p3 0.8 p2 0.5 p2  1  p3 -0.8 p2 -0.5 p2  -1 p3 0.8 p2 0.5 p2  1 p3  0.0 p2  111 p2 111 p3 111 p2 111 p2 111 p3  111 p2  111 p2 111 p3 111 p2 111 p2 111 p3  ',
            '-0.8 p2 -0.5 p2  -1 p3 0.8 p2 0.5 p2  1  p3 -0.8 p2 -0.5 p2  -1 p3 0.8 p2 0.5 p2  1 p3  0.0 p2  111 p2 111 p3 111 p2 111 p2 111 p3  111 p2  111 p2 111 p3 111 p2 111 p2 111 p3  ',
            ' 0.0 p2  111 p2 111 p3 111 p2 111 p2 111 p3 111 p2  111 p2 111 p3 111 p2 111 p2 111 p3 -0.8 p2 -0.5 p2  -1 p3 0.8 p2 0.5 p2  1 p3  -0.8 p2 -0.5 p2  -1 p3 0.8 p2 0.5 p2  1 p3 ',
            ' 0.0 p2  111 p2 111 p3 111 p2 111 p2 111 p3 111 p2  111 p2 111 p3 111 p2 111 p2 111 p3 -0.8 p2 -0.5 p2  -1 p3 0.8 p2 0.5 p2  1 p3  -0.8 p2 -0.5 p2  -1 p3 0.8 p2 0.5 p2  1 p3 ',
          ]
      },
    'whistle2': {
        'sound': 'whistle2.wav',
        'repeat': 3,
        'dance': [
            #                                                        |                                                       |                                                        |
            '0.8 p3 111 p3 111 p3 111 p3 111 p3 111 p3 111 p3 111 p3  0.0 p3 111 p3 111 p3 111 p3 111 p3 111 p3 111 p3 111 p3 -0.8 p3 111 p3 111 p3 111 p3 111 p3 111 p3 111 p3 111 p3 0.0 p3 111 p3 111 p3 111 p3 111 p3 111 p3 111 p3 111 p3',
            '0.0 p3 111 p3 0.8 p3 111 p3 111 p3 111 p3 111 p3 111 p3  111 p3 111 p3 0.0 p3 111 p3 111 p3 111 p3 111 p3 111 p3 111 p3 111 p3 -0.8 p3 111 p3 111 p3 111 p3 111 p3 111 p3 111 p3 111 p3 0.0 p3 111 p3 111 p3 111 p3 111 p3 111 p3',
            '0.0 p3 111 p3 111 p3 111 p3 0.8 p3 111 p3 111 p3 111 p3  111 p3 111 p3 111 p3 111 p3 0.0 p3 111 p3 111 p3 111 p3 111 p3 111 p3 111 p3 111 p3 -0.8 p3 111 p3 111 p3 111 p3 111 p3 111 p3 111 p3 111 p3 0.0 p3 111 p3 111 p3 111 p3',
            '0.0 p3 111 p3 111 p3 111 p3 111 p3 111 p3 0.8 p3 111 p3  111 p3 111 p3 111 p3 111 p3 111 p3 111 p3 0.0 p3 111 p3 111 p3 111 p3 111 p3 111 p3 111 p3 111 p3 -0.8 p3 111 p3 111 p3 111 p3 111 p3 111 p3 111 p3 111 p3 0.0 p3 111 p3'
          ]
      },
    'pam': {
        'sound': 'pam.wav',
        'repeat': 3,
        'dance': [
            '0.9 p3 111 p3 0.0 p3 111 p3 111 p3 111 p3 111 p3 111 p3  -0.9 p3 111 p3 0.0 p3 111 p3 111 p3 111 p3 111 p3 111 p3',
            '0.0 p3 111 p3 111 p3 111 p3 0.9 p3 111 p3 0.0 p3 111 p3  0.0 p3 111 p3 111 p3 111 p3 -0.9 p3 111 p3 0.0 p3 111 p3',
            '0.0 p3 111 p3 111 p3 111 p3 0.9 p3 111 p3 0.0 p3 111 p3  0.0 p3 111 p3 111 p3 111 p3 -0.9 p3 111 p3 0.0 p3 111 p3',
            '0.9 p3 111 p3 0.0 p3 111 p3 111 p3 111 p3 111 p3 111 p3  -0.9 p3 111 p3 0.0 p3 111 p3 111 p3 111 p3 111 p3 111 p3',
          ]
      },
    'kazzoo1': {
        'sound': 'kazzoo1.wav',
        'repeat': 2,
        'dance': [
            #                           |                           |                           |                           |                            #                           |                           |                           |                           |
            '0.3 p1 111 p1 111 p1 111 p1 0.6 p1 111 p1 111 p1 111 p1 0.9 p1 111 p1 111 p1 111 p1 0.6 p1 111 p1 111 p1 111 p1 0.3 p1 111 p1 111 p1 111 p1   -0.3 p1 111 p1 111 p1 111 p1 -0.6 p1 111 p1 111 p1 111 p1 -0.9 p1 111 p1 111 p1 111 p1 -0.6 p1 111 p1 111 p1 111 p1 -0.3 p1 111 p1 111 p1 111 p1    ',
            '111 p1 0.3 p1 111 p1 111 p1 111 p1 0.6 p1 111 p1 111 p1 111 p1 0.9 p1 111 p1 111 p1 111 p1 0.6 p1 111 p1 111 p1 111 p1 0.3 p1 111 p1 111 p1   111 p1 -0.3 p1 111 p1 111 p1 111 p1 -0.6 p1 111 p1 111 p1 111 p1 -0.9 p1 111 p1 111 p1 111 p1 -0.6 p1 111 p1 111 p1 111 p1 -0.3 p1 111 p1 111 p1    ',
            '111 p1 111 p1 0.3 p1 111 p1 111 p1 111 p1 0.6 p1 111 p1 111 p1 111 p1 0.9 p1 111 p1 111 p1 111 p1 0.6 p1 111 p1 111 p1 111 p1 0.3 p1 111 p1   111 p1 111 p1 -0.3 p1 111 p1 111 p1 111 p1 -0.6 p1 111 p1 111 p1 111 p1 -0.9 p1 111 p1 111 p1 111 p1 -0.6 p1 111 p1 111 p1 111 p1 -0.3 p1 111 p1    ',
            '111 p1 111 p1 111 p1 0.3 p1 111 p1 111 p1 111 p1 0.6 p1 111 p1 111 p1 111 p1 0.9 p1 111 p1 111 p1 111 p1 0.6 p1 111 p1 111 p1 111 p1 0.3 p1   111 p1 111 p1 111 p1 -0.3 p1 111 p1 111 p1 111 p1 -0.6 p1 111 p1 111 p1 111 p1 -0.9 p1 111 p1 111 p1 111 p1 -0.6 p1 111 p1 111 p1 111 p1 -0.3 p1    ',
          ]
      }

  }

def reload_config(signal=None, frame=None):
    global config
    try:
        config = settings.load_json(settings.SETTINGS)
        print("settings reloaded for process %s"%(multiprocessing.current_process().name,))
        return True
    except ValueError:
        stop_all = True
        print("Error: can't load settings file")
        return False


def signal_handler(signal, frame):
    global stop_all
    print("interrupt signal received")
    stop_all = True


def servo(q, exit_evt, reload_evt):
    pi = pigpio.pi()

    playing = False
    pause_quant = 0.2
    sng = None
    cor = None
    stop = 0
    idxs = [-1, -1, -1, -1]
    cont = [time.time(), time.time(), time.time(), time.time()]
    repeats = 0
    empty_cnt = 0

    # reset motor positions
    for ss in sn:
        s = servos[ss]
        pi.set_servo_pulsewidth(s['pin'], 1500)
    time.sleep(0.5)
    for ss in sn:
        s = servos[ss]
        pi.set_servo_pulsewidth(s['pin'], 0)

    while True:
        try:
            d = q.get(block=True, timeout=0.1)
            if d==-1:
                empty_cnt += 1
                if empty_cnt>30 and not playing:
                    empty_cnt = 0
                    #start the dance
                    sng = dances[random.choice(dances.keys())]
                    cor = [
                        re.split(' +', sng['dance'][0]),
                        re.split(' +', sng['dance'][1]),
                        re.split(' +', sng['dance'][2]),
                        re.split(' +', sng['dance'][3])
                      ]

                    stop = 0
                    idxs = [-1, -1, -1, -1]
                    cont = [time.time(), time.time(), time.time(), time.time()]
                    repeats = 0
                    # play sound
                    os.system('aplay '+settings.BASEPATH+'sounds/'+sng['sound']+' &')
                    playing = True
            else:
                empty_cnt = 0
                if playing:
                    playing = False
                    #stop
                    os.system('sudo killall aplay')
                    # detach motors
                    for ss in sn:
                        s = servos[ss]
                        pi.set_servo_pulsewidth(s['pin'], 1500)
                    time.sleep(0.5)
                    for ss in sn:
                        s = servos[ss]
                        pi.set_servo_pulsewidth(s['pin'], 0)

        except Empty:
            time.sleep(0.1)
        if exit_evt.is_set():
            print("servo exiting")
            break
        if reload_evt.is_set():
            reload_config()

        # dance start

        if playing:
            if stop==4:
                repeats += 1
                if repeats>int(sng['repeat']):
                    playing = False
                    empty_cnt = 0
                    # stop
                    os.system('sudo killall aplay')
                    # detach motors
                    for ss in sn:
                        s = servos[ss]
                        pi.set_servo_pulsewidth(s['pin'], 1500)
                    time.sleep(0.5)
                    for ss in sn:
                        s = servos[ss]
                        pi.set_servo_pulsewidth(s['pin'], 0)
                else:
                    stop = 0
                    idxs = [-1, -1, -1, -1]
                    cont = [time.time(), time.time(), time.time(), time.time()]

            tm = time.time()
            for si in range(4):
                if tm>cont[si]:
                    idxs[si] += 1
                    s = servos[sn[si]]
                    if idxs[si]>=len(cor[si]):
                        stop += 1
                    else:
                        el = cor[si][idxs[si]].strip()
                        if len(el)>0:
                            if el[0]=='p':
                                # create a pause
                                cont[si] = tm+float(el[1:])*pause_quant
                            else:
                                # add cmd
                                pos = float(el)
                                if s['invert']:
                                    pos *= -1
                                if pos<-1 or pos>1:
                                    final = 0
                                else:
                                    final = (s['max']-s['min'])*(pos/2.0+0.5)+s['min']
                                pi.set_servo_pulsewidth(s['pin'], final)
                                #print("pin: %d, pos: %.2f, final: %d"%(s['pin'], pos, final))



        #dance end



    os.system('sudo killall aplay')
    # detach motors
    for ss in sn:
        s = servos[ss]
        pi.set_servo_pulsewidth(s['pin'], 1500)
    time.sleep(0.5)
    for ss in sn:
        s = servos[ss]
        pi.set_servo_pulsewidth(s['pin'], 0)

    print("servo done")

def proc(data, servoq, exit_evt, reload_evt):
    print("processing started")
    global config
    faceCascade = cv2.CascadeClassifier(config['cascade'])
    last_num_faces=0
    while True:
        try:
            d = data.get(block=True, timeout=0.1)
            gray = cv2.cvtColor(d, cv2.COLOR_BGR2GRAY)

            faces = faceCascade.detectMultiScale(
                gray,
                scaleFactor=float(config['scale_factor']),
                minNeighbors=int(config['min_neighbors']),
                minSize=(int(config['min_size']), int(config['min_size'])),
                flags=cv2.CASCADE_SCALE_IMAGE
            )

            f = []
            for (x, y, w, h) in faces:
                print(x, y, w, h)
                f.append({
                    'x': float(x)/float(config['width']), 
                    'y': float(y)/float(config['height']), 
                    'w': float(w)/float(config['width']), 
                    'h': float(h)/float(config['height'])
                  })
            settings.save_json(settings.FACES_JSON, f)

            if len(faces)>0 and last_num_faces>0:
                servoq.put(len(faces))
            else:
                servoq.put(-1)
            last_num_faces = len(faces)

        except Empty:
            time.sleep(0.1)
        
        if exit_evt.is_set():
            print("proc exiting")
            break

        if reload_evt.is_set():
            reload_config()

    print("proc done")


def capt(data, exit_evt, reload_evt):
    print("capture process started")
    global config
    # initialize the camera and grab a reference to the raw camera capture
    camera = PiCamera()
    camera.resolution = (int(config['width']), int(config['height']))
    camera.framerate = 15
    rawCapture = PiRGBArray(camera, size=(int(config['width']), int(config['height'])))
    # allow the camera to warmup
    time.sleep(0.5)
    _tm=time.time()
    for frame in camera.capture_continuous(rawCapture, format="bgr", use_video_port=True):
        image = frame.array

        ## show the frame
        #cv2.imshow("Frame", image)
        #key = cv2.waitKey(1) & 0xFF
        ## if the `q` key was pressed, break from the loop
        #if key == ord("q"):
        #    break

        try:
            data.put(image, block=True, timeout=0.1)
            #print("capt added to queue")
        except Full:
            #print("capt queue full, skipping frame")
            pass

        #print("frame %.3f"%(time.time()-_tm,))
        _tm=time.time()

        # clear the stream in preparation for the next frame
        rawCapture.truncate(0)

        if exit_evt.is_set():
            print("capt exiting")
            break

        if reload_evt.is_set():
            reload_config()

    rawCapture.truncate(0)
    rawCapture.close()
    camera.close()
    print("capt done")

def main():
    global stop_all

    if not reload_config():
        print("ERR")
        sys.exit(-3)

    # children processes should ignore the SIGINT
    signal.signal(signal.SIGINT, signal.SIG_IGN)
    
    # but they should reload settings on SIGUSR1
    signal.signal(signal.SIGUSR1, reload_config)

    data_q = multiprocessing.Queue(1)
    servo_q = multiprocessing.Queue()
    exit_evt = multiprocessing.Event()
    reload_evt = multiprocessing.Event()
    pc = multiprocessing.Process(target=capt, args=(data_q,exit_evt,reload_evt))
    pp = multiprocessing.Process(target=proc, args=(data_q,servo_q,exit_evt,reload_evt))
    ps = multiprocessing.Process(target=servo, args=(servo_q,exit_evt,reload_evt))

    pc.start()
    pp.start()
    ps.start()

    os.system('amixer -- sset PCM playback -10dB')
    os.system('aplay '+settings.BASEPATH+'sounds/kazzoo_startup.wav &')
    time.sleep(1)

    # parent should react to signal
    signal.signal(signal.SIGINT, signal_handler)

    while True:
        time.sleep(0.2)

        if stop_all:
            exit_evt.set()

            pp.join()
            print("proc joined")
            ps.join()
            print("servo joined")
            time.sleep(1)
            pc.terminate()  # capture process hangs...
            pc.join()
            print("capt joined")
            break


if __name__=="__main__":
    main()
