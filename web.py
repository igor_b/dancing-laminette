#!/usr/bin/env python2
# -*- coding: utf-8 -*-

# This file is part of the "Dancing Laminette" interactive installation.
# It is released under MIT license
# Copyright (c) 2016 by Igor Brkić <igor@hyperglitch.com>

from __future__ import absolute_import, division, print_function, unicode_literals

import subprocess
import time
import os
import bottle
from bottle import route, view, request
bottle.TEMPLATE_PATH.append('assets')

import random
import json
import re
import settings

app = application = bottle.Bottle()

class SupervisorNotSet(Exception):
    pass

def is_running():
    out = subprocess.check_output(('sudo', 'supervisorctl', 'status')).split('\n')
    for o in out:
        ln = re.split(' +', o)
        if ln[0]=='chair': 
            return ln[1]=='RUNNING'
    raise SupervisorNotSet
            

@app.get('/mode')
def get_status():
    out = subprocess.check_output('mount').split('\n')
    for o in out:
        ln = re.split(' +', o)
        if ln[2]=='/': 
            if 'rw' in ln[5]:
                return 'read-write'
            else:
                return 'read-only'
    return 'ERROR'


@app.get('/mode/<m>')
def get_status(m):
    if m=='rw':
        os.system('sudo mount -o remount,rw /')
    else:
        os.system('sudo mount -o remount,ro /')
    return 'read-only'



@app.get('/status')
def get_status():
    if is_running():
        return 'running'
    else:
        return 'stopped'

@app.get('/faces')
def get_faces():
    try:
        with open(settings.FACES_JSON, "r") as f:
            faces = json.load(f)
    except (IOError, ValueError):
        faces = []
    return {'d': faces}


@app.get('/still')
def get_still():
    os.system(settings.BASEPATH+'singleshot.py')
    return '/still/still.jpg?tm=%d&rnd=%d'%(int(time.time()), int(random.random()*10000))


@app.get('/still/<filename:path>')
def file_get(filename):
    return bottle.static_file(filename, root='/tmp/')


@app.get('/static/<filename:path>')
def file_get(filename):
    return bottle.static_file(filename, root=settings.BASEPATH+'assets')


@app.get('/start')
def start():
    subprocess.call(('sudo', 'supervisorctl', 'start', 'chair'))
    return get_status()


@app.get('/stop')
def stop():
    subprocess.call(('sudo', 'supervisorctl', 'stop', 'chair'))
    return get_status()


@app.get('/advanced')
@view('advanced')
def get_advanced():
    try:
        return settings.load_json(settings.SETTINGS)
    except ValueError:
        return {}


@app.post('/advanced')
@view('advanced')
def post_advanced():
    try:
        sett = settings.load_json(settings.SETTINGS)
    except ValueError:
        sett = {}
    for k in ('width', 'height', 'cascade', 'scale_factor', 'min_neighbors', 'min_size'):
        sett[k] = request.POST.get(k, sett.get(k, ''))

    settings.save_json(settings.SETTINGS, sett)
    sett['msg'] = "Settings saved"
    return sett


@app.route('/')
@view('index')
def index():
    d = {'status': 'running'}
    return d


if __name__=="__main__":
    #bottle.debug(True)
    bottle.run(app=app, host='0.0.0.0', port=8080, reloader=True)

