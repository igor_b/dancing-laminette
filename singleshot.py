#!/usr/bin/env python
# -*- coding: utf-8 -*-

# TODO: add info and license

from picamera.array import PiRGBArray
from picamera import PiCamera
import time
import cv2
import sys
import settings


def oneshot(config):
    camera = PiCamera()
    camera.resolution = (int(config['width']), int(config['height']))
    camera.framerate = 15
    rawCapture = PiRGBArray(camera, size=(int(config['width']), int(config['height'])))
    faceCascade = cv2.CascadeClassifier(config['cascade'])
    # allow the camera to warmup
    time.sleep(0.5)
    for frame in camera.capture_continuous(rawCapture, format="bgr", use_video_port=True):
        image = frame.array
        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

        faces = faceCascade.detectMultiScale(
            gray,
            scaleFactor=float(config['scale_factor']),
            minNeighbors=int(config['min_neighbors']),
            minSize=(int(config['min_size']), int(config['min_size'])),
            flags=cv2.CASCADE_SCALE_IMAGE
        )

        f = []
        for (x, y, w, h) in faces:
            cv2.rectangle(image, (x, y), (x+w, y+h), (0, 255, 0), 2)

        camera.close()
        cv2.imwrite('/tmp/still.jpg', image)

        break

if __name__=="__main__":
    config = settings.load_json(settings.SETTINGS)
    oneshot(config)

